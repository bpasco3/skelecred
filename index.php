<?php
// Skelecred (c) William Pascoe, 2016
    include './admin/PublicCredify.php';
    include './admin/Template.php';
    include './admin/DBConn.php';
    include './admin/Filters.php';
    include './admin/PublicContext.php';
    include './admin/Paging.php';

    $template = new Template();

  //  $user = $template->CheckAccess();

    $dbconn = new DBConn();
    $tn = isset($_GET['t']) ? $_GET['t'] : null;
    $aTable = new TableDetails($tn, $dbconn);

    $aFilter = new Filters();

   // $user->SetClause($aTable, $dbconn);

   // $aFilter->AddUser($user);
    if (isset($_GET['r'])){
        $aFilter->AddFilterPair( $aTable->primaryKey, $_GET['r']);
    }
    else if (isset($_GET['f']) && isset($_GET['fk'])){
        // need to include tablename to avoid ambiguous references in SQL
        $aFilter->AddFilterPair( $aTable->tablename . "." . $_GET['f'], $_GET['fk']);
    }
    $cred = new PublicCredify( $dbconn, $aTable, $aFilter );
    $template->Header($aTable->displayTablename());

    $context = new PublicContext($dbconn);

    $context->show($aTable, $aFilter);


    
        echo("<p><a href='./read.php?t=" . $aTable->tablename . "'>Show All</a></p>");
    

    echo '<table id="readtable" class="admin table">';
    
    $cred->AllColumns();

    $page = new Paging($dbconn->GetRecordCount($aTable, $aFilter));
    if( isset($_GET{'page'}) ) {
                 $page->ResetPage($_GET{'page'});
    }

    $cred->AllRows($page);

    echo '</table>';



    $page->PageControls($_SERVER['QUERY_STRING']);
    $template->Footer();
?>
