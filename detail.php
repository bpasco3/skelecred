<?php
    include './admin/PublicCredify.php';
    include './admin/Template.php';
    include './admin/DBConn.php';
    include './admin/Filters.php';
    include './admin/PublicContext.php';
    
    $template = new Template();
    
    //$user = $template->CheckAccess();
        
    $dbconn = new DBConn();

    $aTable = new TableDetails($_GET['t'], $dbconn);

    $aFilter = new Filters();
  //  $user->SetClause($aTable, $dbconn);
  //  $aFilter->AddUser($user);
    
    $aFilter->AddFilterPair($aTable->primaryKey, isset($_GET['r']) ? $_GET['r'] : '');
        
    $crud = new PublicCredify( $dbconn, $aTable, $aFilter );
    
    $context = new PublicContext($dbconn);
    $subtitle = $context->getRowTitle($aTable, $aFilter);
    $template->Header($aTable->displayTablename() . ($subtitle ? ": " . $subtitle : ""));
    
    $context->show($aTable, $aFilter);
    echo "<div class='warnings'><p></p></div>";
    echo "<div id='details' class='clearfix'>";
    $crud->Details($_GET['r']);
    echo "</div>";
    
    $template->Footer();
?>
