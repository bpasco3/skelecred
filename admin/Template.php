<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('Australia/NSW');

include "access/User.php";

class Template {
    public function __construct () {
        //var_dump(session_status());
       // if there is no session start one.
       if (session_status() !=2 ) {
        session_start();
       }
     //  var_dump(session_status());
    }
    function Header($title) {

    
        echo "<html>
    <head>
        <link rel='stylesheet' type='text/css' id='mainstyle' href='/admin/skelecred.css'>
        <head>
        <meta charset='UTF-8'>
                
        <style id='antiClickjack'>body{display:none !important;}</style>

        <script type='text/javascript'>
           if (self === top) {
               var antiClickjack = document.getElementById('antiClickjack');
               antiClickjack.parentNode.removeChild(antiClickjack);
           } else {
               top.location = self.location;
           }
        </script>
        
            <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
            <script src='skelecred.js'></script>
            <script src='skelecredcore.js'></script>
        </head>
        <title>Skelecred</title>
    </head>
<body>
<div id='header' class='clearfix'>
<h1 class='clearfix'>SKELECRED</h1>
<div id='nav1'>
<p><span> <a href='index.php'>HOME</a></span>
<span><a href='./master.php'>MANAGE</a></span>
";

if (
    isset($_SESSION['user']) && !empty($_SESSION['user']) &&
    isset($_SESSION['authentic']) && $_SESSION['authentic'] === "1"
   ) {
    echo "<span> <a href='logout.php'>LOG OUT</a>
   </span>
   ";
} else {
 echo "<span> <a href='login.php'>LOG IN</a>
</span>
";
}

echo "
 <span> <a href='help.php'>HELP</a> </span>
</p>";
//session_start();

echo "
</div>

</ul>
</div>
</div>

<div id='content'>

";
        if (isset($title)) {
            echo("<h2>" . $title . "</h2>");
        }
    }
    
    // also wrapping Check XSS in here, because it's a prelimary check and bail out before anything else scenario.
    function CheckAccess() {
     $this->CheckXSS();
     //var_dump($_SESSION);
     // user access controls
     if (!(
         isset($_SESSION['iduser']) && !empty($_SESSION['iduser']) &&
         isset($_SESSION['user']) && !empty($_SESSION['user']) &&
         isset($_SESSION['authentic']) && $_SESSION['authentic'] === "1" &&
         isset($_SESSION['role']) && !empty($_SESSION['role']) 
        )) {
        //$this->Header("Log in required.");
        echo "<html><head><title>prototype, access restricted</title></head><body>";
           echo "<p>Please <a href='login.php'>log in</a> to access this page.</p>
          ";
       
        $this->Footer();
        exit;
     }
     if ($_SESSION['role'] !== "admin" && isset($_GET["t"])) { // block access to restricted pages
      if (($_GET["t"] === "help" || $_GET["t"] === "page" || $_GET["t"] === "steptype")) {
       echo "<p>Admin access only.</p>";
       exit;
      }

     }
     return new User($_SESSION['iduser'], $_SESSION['user'],$_SESSION['role']);
    }
    
    function CheckXSS(){
     $xssok = TRUE;

     
     if (!(preg_match("/.php$/i",$_SERVER['PHP_SELF']) || preg_match("/.php?t=/i",$_SERVER['PHP_SELF']))) {
      $xssok = FALSE;
     }
     
     if (!(empty($_SERVER['QUERY_STRING']) || substr( $_SERVER['QUERY_STRING'], 0, 2 ) === "t=" )) {
      $xssok = FALSE;
     }
     
     if (isset($_GET["un"]) ) {
      if (!is_numeric($_GET["un"])) {
       $xssok = FALSE;
      }
     }
     if (isset($_GET["r"]) ) {
      if (!is_numeric($_GET["r"])) {
       $xssok = FALSE;
      }
     }
     if (isset($_GET["fk"]) ) {
      if (!is_numeric($_GET["fk"])) {
       $xssok = FALSE;
      }
     }
     if (isset($_GET["b"]) ) {
      if (preg_match("/[^a-zA-Z0-9\-.$\/\%+]/", $_GET["b"])) {
       $xssok = FALSE;
      }
     }
     if (isset($_GET["f"]) ) {
      if (!ctype_alnum($_GET["f"])) {
       $xssok = FALSE;
      }
     }
     if (isset($_GET["t"]) ) {
      $alsoValidCharacters = array('-', '_');
      if (!ctype_alnum(str_replace($alsoValidCharacters, '', $_GET["t"]))) {
        $xssok = FALSE;
      }
     }
     if (isset($_GET["page"]) ) {
      if (!is_numeric($_GET["page"])) {
       $xssok = FALSE;
      }
     }
     foreach ($_GET as $key => $value) {
      if (!($key === "t" || $key === "r" || $key === "b" || $key === "f" || $key === "fk" || $key === "un" || $key === "page")) {
              $xssok = FALSE;
      }
     }
     if (!$xssok) {
      echo "<p>Potential security problem detected. Your IP addressed may be logged as a result of this incident. If you did not expect to see this message please return to the home page and try again.</p>
          ";
          exit;
     }
    }
    function Footer() {
        echo "
        </div>
        <div id='footer'>
        </body>
        </html>";
    }
}
?>