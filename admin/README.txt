README

Skelecred is a lean, instant CRED (or CRUD) web interface to database with PHP. Just upload to your server, configure the database connection and instantly create, read, edit (update) and delete records on the web, incl links for foreign keys. It's great for rapid prototyping and Agile database driven web develoment. This also includes linking among tables joined by foreign key.

Unlike most automatic CRED generators Skelecred automatically generates everything on the fly, rather than writing out files to customise. Skelecred is OO so to customise just inherit the classes. The Skelecred design philosophy is to start with a simple but effective minimum to get everything working, which you can then build on. This is in contrast to many other frameworks which include libraries and architecture to cater for all scenarios which can lead to bloatware, especially for small projects.

Usage
1. Place all the files where you want them on a webserver. This is mainly to provide a web admin interface to a database, which drives an end user interface. It enables changes to the data in the whole database, so it's a 'superuser' level admin tool. For these reasons you might want to put it into an '/admin' folder that has access restricted by .htaccess. You will certainly need some sort of security around who can access, because it provides full access to all data in the database.
2. Configure the connection in DBConn.php
3. In your web browser navigate to the www.adomain.com/whereyouputthefiles/master.php
4. You should see a list of the tables in your database, which you can click on to create, read, edit and delete from.

Skelecrud is Object Oriented so the best way to customise from here on is the create a class that inherits the class you wish to customise. Then you can add to or change behaviour without changing the original code. If things go wrong you will still have the original files and URLs for simple superuser access to the database.

The Architecture is in layers. The files are named with no capitals for pages users will navigate to, and capitals for other classes etc.

1. master.php, create.php, read.php, edit.php, delete.php are pages the user will navigate to. They are very simple. The create 'template' objects for HTML headers and footers, and create the relevant DBConnector for dependency injection into Credify. It calls Credify methods to build the main contents of the page. These front end files also include references to Javascript and JQuery for front end functionality.

2. Credify.php contains all the server side application level functionality, such as listing all rows in a table for 'read.php', modifying query strings into filters to pass to DBConnector to return or edit specific rows etc. This layer also wraps results in HTML for structured elements to be shown on the page. The HTML is for structure only, not design which should be done through the .css files. HTML elements are well organised and labelled with id tags and classes to enable full control of their appearance through CSS, and full front end interface control through Javascript and JQuery.

3. DBConnector.php contains all the database relevant methods, in particular running specific queries given filters etc provided by Credify. This layer is for DB connectivity only and contains no HTML.
So for example, if you wanted to add some special database queries to your application you would create a new MyDBConnector.php class that inherits from DBConnector.php and pass that into Credify.php. In your MyDBConnect.php class you would add or override methods. Then you would probably want to special methods in Credify.php to handle the results, so, create a MyCredify.php class that inherits Credify.php and pass the MyDBConnect object to your MyCredify object instead of a Credify object. 

Fees and Licencing

Skelecred can be used for education and personal, not for profit purposes free of charge. You can none the less donate money to support the creators. If skelecred is used in a profit making activity you must pay a reasonable fee, depending on the scale of your activity. Skelecred may not be included in any other software or services for commercial use without prior consent of the creator.

You can modify this code to suit your ends, but bear in mind the recommended usage is to not modify the core classes but to create classes inheriting the existing ones and customise those. If the code is changed you must cite 'Skelecrud' as the title and 'Bill Pascoe' as the author. It's easiest just to leave the existing copyright notice.

Security

Security remains your responsibility. The creators of Skelecred accept no responsibility for the security of your data. Some common practices have been implemented for security but there are flaws in any system and different systems require different levels of security. You must satisfy yourself whether skelecred is secure enough and/or implement your own security changes and improvements. Be aware that any security issues relate to issues outside the code such as your web and database servers and networks. We would like to hear of any security holes you find in Skelecred.

Disclaimer

This software is provided "as is" without warranty. By using this software you absolve the creators of Skelecred of any liability. You will not hold the creators responsible for any problems arising from using it. This software is provided in good faith to assist you.

