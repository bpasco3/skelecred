<?php

// Manually set up mappings from tablenames and column names to the names and information to actually display (with capital letters, allowing spaces, saying units of measure etc.
// For mapping the SQL column type to an HTML input type

// maybe make it all static

class Display {
    
    public $m = []; // user friendly name for tables, columns and a special one for which column to use as a 'title' eg on a create or edit page.
    public $title = []; // for the html input element title attribute
    public $place = []; // for the html input element title attribute
    public $type = []; // for the html input element title attribute
    public $defaultTable = [];
    
    /*
     * the following key value pairs all should match column names in the database with the exception of 'tablename'and 'titlefield'
     * 'tablename' is the display value for the table as a whole, eg: the table might be called 'projects' but we want it to appear as 'My Projects'
     * 'titlefield' is to specify which column to get a 'title' value from. Eg: the title of the project page should not just be 'Projects' but
     * the specific name of this project, which might be contained in a 'projectname' or simillar field. This is relevant if the table is filtered.
     */
    // Todo: maybe change all these to be like a decorator object (ie: TableDetails) for the table and each column in the table
    public function __construct () {
        
        $this->defaultTable = "test"; //table to show user if no table is selected. This would be a main, root, starting table.

        // INPUT FIELD VALUES
        $this->title = array(
            "text" => "Text",
            "varchar(255)" => "Text",
            "float" => "Number with decimal places.",
            "int(11)" => "Integer",
            "int(12)" => "Integer",
            "tinyint(1)" => "Select",
            "tinyint(2)" => "Two Digit Integer",
            "tinyint(4)" => "Four Digit Integer",
            "datetime" => "Date and Time in format YYYY-MM-DD HH:MM:SS",
            "time" => "Elapsed time (not a time of day) in format HH:MM:SS"
            );
        $this->place = array(
            "text" => "Text",
            "varchar(255)" => "Text",
            "float" => "Number",
            "int(11)" => "Whole Number",
            "int(12)" => "Whole Number",
            "tinyint(1)" => "Select",
            "tinyint(2)" => "Two Digit Number",
            "tinyint(4)" => "Four Digit Number",
            "datetime" => "YYYY-MM-DD HH:MM:SS",
            "time" => "HH:MM:SS"
        );
        $this->type = array(
            "text" => "textarea",
            "varchar(255)" => "text",
            "float" => "text",
            "int(11)" => "text",
            "int(12)" => "text",
            "tinyint(1)" => "checkbox",
            "tinyint(2)" => "text",
            "tinyint(4)" => "text",
            "datetime" => "text",
            "time" => "text"
        );
        
                    // Only show these tables.
        // With security restrictions there is always a decision whether to show unless specifically hidden or hide unless specifically shown.
        // Here we err on the side of caution, and require that tables be specifically made available.
        $this->showtables = array(
            "test",
            "user"
        );
        
        // basic table info
        $this->table["user"] = [
            "tablename" => "Users", // name of the table
            "titlefield" => "displayname" // which field should be used as the title of a page?
        ];
        $this->hide["user"] = [
            "institute",
            "department",
            "groupleader",
            "departmenthead"
            ];

        // DISPLAY FIELD NAMES

        $this->m["user"] = [
            "username" => "User Name",
            "iduser" => "Details",
            "displayname" => "Display Name",
            "pw" => "Password",
            "active" => "Account Active",
            "country" => "Country",
            "contactemail" => "Email",
            "contactphone" => "Phone",
            "contactmobile" => "Mobile"
        ];
        $this->m["test"] = [
            "name" => "Name",
            "description" => "Description",
            "temperature" => "Temperature",
            "timehours" => "Time Hours",
            "timeduration" => "Step Duration"
            ];
    // DEFAULT VALUES

        $this->def["test"] = [
            "temperature" => "37",
            "timehours" => "00:00:00",
            "timeduration" => "96:00:00"
            ];
        
    }
}







?>