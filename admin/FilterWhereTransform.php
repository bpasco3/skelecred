<?php

class FilterWhereTransform {

    public $fields = '';
    public $values = [];

    public function __construct ($query) {
            if ( !empty($query)) {
                $this->values = array_values($query);
                $this->Nullify($this->values);
                $afields = array_keys($query);
                foreach ($afields as &$key) {
                    $key = $key . " = ? ";
                }
                $this->fields = implode(" , ", $afields);
            }
    }

        function Nullify(&$listofvalues) {
                foreach($listofvalues as &$listitem) {
                        if ($listitem == NULL) { // == regards empty string as Null. We need to set empty string to actually be null, so that integer fields get updated to null instead of an empty string which gives an error.
                                $listitem = NULL;
                        }
                }
        }
    
}
?>