<?php
// Skelecred (c) William Pascoe, 2016

    include 'CredifyCore.php';
    
class PublicCredify extends CredifyCore {
    
    function AllRows($paging) {

            $aDisplay = $this->aDisplay;

        foreach ($this->dbConnect->GetAllRows($this->table, $this->filter, $paging) as $row) {

                echo ("\n<tr>\n");
                foreach ($row as $key => $field) {
                       // echo $key;
                        if ($this->table->hideInList($key)) {
                                continue;
                        }
                
                        
                        $FKto = $this->table->IsFKTo($key); //trying to minimise calls to database, and to the check
                        if ($FKto) {
                                reset($FKto);
                                list($totable, $tocol) = each($FKto);
                                $FKDisplay = isset($aDisplay->table[$totable]["tablename"]) ? $aDisplay->table[$totable]["tablename"] : $totable;
                                $field = "<a href='./index.php?t=" . $totable . "&f=" . $tocol . "&fk=" . $field . "'>" . $FKDisplay . "</a>";
                        }
                        $FKfrom = $this->table->IsFKFrom($key);
                        $linksTo = "";
                        if ($FKfrom) { 
                                        foreach ($FKfrom as $fromrow ){
                                                foreach ($fromrow as $pair => $pairval) {
                                                        $tDisplay = isset($aDisplay->table[$pair]["tablename"]) ? $aDisplay->table[$pair]["tablename"] : $pair;
                                                        $linksTo = $linksTo . "<a href='./index.php?t=" . $pair . "&f=" . $pairval . "&fk=" . $row[$this->table->primaryKey] . "'>" . $tDisplay . "</a> ";
                                                }
                                        }
                                
                               $field = $linksTo;
                        }
                        if ($key === "pw") {
                                $field = "********";
                        }
                    //echo "<td>" . $field ." " . $linksTo . " " . $FKto . "</td>";
                    
                        echo "<td>" . $field . "</td>";

                }

                echo ("
                        <td class='details'>" . $this->detailSnippet($this->table->tablename, $row[$this->table->primaryKey]) . "</td>
                        </tr>
                        ");
                
        }
    }

}
?>