<?php
// Skelecred (c) William Pascoe, 2016
include 'TableDetails.php';
include_once 'Display.php';
    
class CredifyCore {
        public $dbConnect;
        public $table;
        public $filter;
        public $aDisplay;

        public function __construct ($dbin, $intable, $infilter) {
                $this->dbConnect = $dbin;
                $this->table = $intable;
                $this->filter = $infilter;
                $this->aDisplay = new Display();
        }

    function AllTables() {
            foreach ($this->dbConnect->GetTables() as $row) {
                if (empty($this->aDisplay->showtables) || in_array(Implode("",$row), $this->aDisplay->showtables)) {
                        $tDisplay = isset($this->aDisplay->table[Implode("",$row)]["tablename"]) ? $this->aDisplay->table[Implode("",$row)]["tablename"] : Implode("",$row);
                        echo '<h3><a href="./read.php?t=' . Implode("",$row) . '">' . $tDisplay . "</a></h3>\n";
                }
            }
    }

    function AllColumns() {
        echo ("\n<tr class='colheading'>\n");
        foreach ($this->table->columnInfo as $row) {
                if ($this->table->hideInList($row["Field"])) {
                        continue;
                }
                echo "<td>" . $this->table->displayCol($row["Field"]) ."</td>";
        }
        /*foreach ($this->dbConnect->GetColumns($this->table) as $row) {
                echo "<td>" . $row["Field"] ."</td>";
        }*/
        echo ("\n</tr>\n");
    }

  //  function AllRows($tablename, $field, $fk) {
    function AllRows($paging) {

            $aDisplay = $this->aDisplay;

        foreach ($this->dbConnect->GetAllRows($this->table, $this->filter, $paging) as $row) {

                echo ("\n<tr>\n");
                foreach ($row as $key => $field) {
                       // echo $key;
                        if ($this->table->hideInList($key)) {
                                continue;
                        }
                
                        
                        $FKto = $this->table->IsFKTo($key); //trying to minimise calls to database, and to the check
                        if ($FKto) {
                                reset($FKto);
                                list($totable, $tocol) = each($FKto);
                                $FKDisplay = isset($aDisplay->table[$totable]["tablename"]) ? $aDisplay->table[$totable]["tablename"] : $totable;
                                $field = "<a href='./read.php?t=" . $totable . "&f=" . $tocol . "&fk=" . $field . "'>" . $FKDisplay . "</a>";
                        }
                        $FKfrom = $this->table->IsFKFrom($key);
                        $linksTo = "";
                        if ($FKfrom) { 
                                        foreach ($FKfrom as $fromrow ){
                                                foreach ($fromrow as $pair => $pairval) {
                                                        $tDisplay = isset($aDisplay->table[$pair]["tablename"]) ? $aDisplay->table[$pair]["tablename"] : $pair;
                                                        $linksTo = $linksTo . "<a href='./read.php?t=" . $pair . "&f=" . $pairval . "&fk=" . $row[$this->table->primaryKey] . "'>" . $tDisplay . "</a> ";
                                                }
                                        }
                                
                               $field = $linksTo;
                        }
                        if ($key === "pw") {
                                $field = "********";
                        }
                    //echo "<td>" . $field ." " . $linksTo . " " . $FKto . "</td>";
                    
                        echo "<td>" . $field . "</td>";

                }

                echo ("
                        <td class='details'>" . $this->detailSnippet($this->table->tablename, $row[$this->table->primaryKey]) . "</td>
                        <td class='edit'>" . $this->editSnippet($this->table->tablename, $row[$this->table->primaryKey]) . "</td>
                        <td class='delete'>" . $this->deleteSnippet($this->table->tablename, $row[$this->table->primaryKey]) . "</td>
                        </tr>
                        ");
                
        }
    }
    

    function DetailSnippet($t, $r){
        return "<a href='detail.php?t=" . $t . "&r=" .$r . "'>detail</a> ";
    }    
    function EditSnippet($t, $r){
        return "<a href='edit.php?t=" . $t . "&r=" .$r . "'>edit</a> ";
    }
    function DeleteSnippet($t, $r){
        return "<a href='./delete.php?t=".$t."&r=" . $r . "'>delete</a> ";
    }
    
        function Details($rowid) {
// TODO: detect type of input and create form accordingly
                $edfields = $this->aDisplay;
                
        //var_dump($this->filter);
                $results = $this->dbConnect->GetRow($this->table, $this->filter);
      //  var_dump($results);
                foreach ($results as $result) {
                        foreach ($this->table->columnInfo as $col) {
                                if ($col["Field"] === "pw"){
                                        echo "<li><label>Password: </label>********</li>";
                                }
                                elseif ($this->table->primaryKey ===  $col["Field"] || $this->table->isFKTo($col["Field"])  || $this->table->IsFKFrom($col["Field"]))
                                    {
                                       // echo "<input type='hidden' name='" . $col["Field"] ."' value='" . $result[$col["Field"]] . "' required></input>";
                                        
                        
                                        $field = $col["Field"];
                                        // set up links to other tables        
                                        $FKto = $this->table->IsFKTo($col["Field"]); 
                                        if ($FKto) {
                                                $FKDisplay = isset($aDisplay->m[$FKto]["tablename"]) ? $aDisplay->m[$FKto]["tablename"] : $FKto;
                                                $FKto = "<a href='./read.php?t=" . $FKto . "&f=" . $col["Field"] . "&fk=" . $result[$col["Field"]] . "'>" . $FKDisplay . "</a>";
                                                $field = $FKto;
                                        }
                                        $FKfrom = $this->table->IsFKFrom($col["Field"]);
                                        $linksTo = "";
                                        if ($FKfrom) { 
                                                        foreach ($FKfrom as $fromrow ){
                                                                foreach ($fromrow as $pair => $pairval) {
                                                                        $tDisplay = isset($aDisplay->m[$pair]["tablename"]) ? $aDisplay->m[$pair]["tablename"] : $pair;
                                                                        $linksTo = $linksTo . "<a href='./read.php?t=" . $pair . "&f=" . $pairval . "&fk=" . $result[$this->table->primaryKey] . "'>$tDisplay</a> ";
                                                                }
                                                        }
                                                $field = $linksTo;
                                                
                                        }
                                        echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> " . $field . "</li>";    
                                        
                                        
                                } else {
                                        if ($edfields->type[$col["Type"]] === "textarea") { // keep thes conditions, in case we want to display large text with different classes, etc.
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> " . $result[$col["Field"]] . "</li>";
                                        }
                                        else if ($edfields->type[$col["Type"]] === "checkbox") {
                                                
                                                $checked = ($result[$col["Field"]] === '1') ? "Yes" : "No";
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> " . $checked . "</li>";
                                        } else {
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> " . $result[$col["Field"]] . "</li>";
                                        }
                                }
                        }
                        echo ("<div class='context'> "
                         . $this->detailSnippet($this->table->tablename, $result[$this->table->primaryKey]) . " | " 
                         . $this->editSnippet($this->table->tablename, $result[$this->table->primaryKey])  . " | " 
                         . $this->deleteSnippet($this->table->tablename, $result[$this->table->primaryKey])
                         ."</div>");
                }
                
                                
    
        }
    
        function CreateFormElements() {
                
                $infields = $this->aDisplay;
                echo ("<form action='create.php?t=" . $this->table->tablename . "' method='POST' class='clearfix'>
                      <ul>");
                $pkey = $this->dbConnect->GetTablePKey($this->table->tablename);
                foreach ($this->table->columnInfo as $col) {
                        // assume auto-incrementn primary key so don't provide it to the insert statement.
                        if (!($col["Field"] === $pkey)){
                                $value = '';
                                $checked = ''; // just for checkboxes
                                if ($this->table->isFKTo($col["Field"])) {
                                        if ($col["Field"] === "iduser") {
                                                $colFieldKey = $this->table->tablename . ".iduser";
                                        } else {
                                        //$colFieldKey = ($col["Field"] === "iduser") ? "user.iduser" : $col["Field"]; // to cater for filtering by user in all cases
                                                $colFieldKey = $this->table->tablename . "." . $col["Field"];
                                        }
                                        if (isset($this->filter->filterpairs[$colFieldKey])) {
                                                $value = $this->filter->filterpairs[$colFieldKey];
                                        } else {
                                                $FKtotable = $this->table->isFKTo($col["Field"]);
                                                reset($FKtotable);
                                                list($totable, $tocol) = each($FKtotable);
                                                echo "<p class='warnings'>WARNING: This record is not linked with a 'parent' record in the table, " . $totable . ". First <a href='read.php?t=" . $totable . "'>navigate to the table</a> and next to the record you want to add this to, click the name of this table. Then you can add this record, linked to a parent. Without a link to a parent the record may not be found.</p>";
                                        }    
                                } else {
                                        $value = $this->table->defaultVal($col["Field"]);
                                        if ($infields->type[$col["Type"]] === "checkbox" && !empty($value)) {
                                                $checked = ($value === '1') ? "checked='checked'" : '';
                                        }
                                }
                                
                                
                                if (
                                    $this->table->primaryKey ===  $col["Field"] || $this->table->isFKTo($col["Field"])
                                    ) {
                                                echo "<input type='hidden' name='" . $col["Field"] ."' value='" . $value . "' required></input>";
                                } else {
                                
                                        if ($infields->type[$col["Type"]] === "textarea") {
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) .
                                                ":</label> <textarea cols='25' rows='9'" .
                                                "' title='" . $infields->title[$col["Type"]] .
                                                "' placeholder='" .$infields->place[$col["Type"]].
                                                "' name='" .
                                                $col["Field"] .
                                                "' value='" .
                                                $value . "'></textarea></li>";
                                        }
                                        else {
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) .
                                                ":</label> <input type='" . $infields->type[$col["Type"]] .
                                                "' title='" . $infields->title[$col["Type"]] .
                                                "' placeholder='" .$infields->place[$col["Type"]].
                                                "' name='" .
                                                $col["Field"] .
                                                "' value='" . $value. "' " . $checked . "></input></li>"; 
                                        }
                                }
                        }
                }
                echo "
               <li><label> &nbsp;</label><input type='submit' value='save'></input></li></ul>
                </form>";
        }
        
        function InsertRow(){
                
                if ( !empty($_POST)) {

                        // assuming auto-increment primary key, so assume the col exists but that it isn't provided as input
                        $pkey = $this->dbConnect->GetTablePKey($this->table->tablename);

                                $results = $this->dbConnect->InsertRow($this->table, $this->filter);
                                $keepUser = $this->filter->user;
                                $this->filter = new Filters();
                                $this->filter->AddUser($keepUser);
                                $this->filter->AddFilterPair($this->table->primaryKey, $results); 

                        return $results;
                }
        }
        
    
        function EditFormElements($rowid) {
// TODO: detect type of input and create form accordingly
                $edfields = $this->aDisplay;
                echo ("
                      <form action='edit.php?t=" . $this->table->tablename . "&r=" . $rowid . "' method='POST' class='clearfix'>
                      <ul>");
        //var_dump($this->filter);
                $results = $this->dbConnect->GetRow($this->table, $this->filter);
      //  var_dump($results);
                foreach ($results as $result) {
                        foreach ($this->table->columnInfo as $col) {
                                if ($col["Field"] === "pw"){
                                        echo "<input type='hidden' name='" . $col["Field"] ."' value='" . $result[$col["Field"]] . "' required></input>";
                                }
                                elseif (
                                    ($this->table->primaryKey ===  $col["Field"] || $this->table->isFKTo($col["Field"])  || $col["Field"] === "active")
                                    && $this->filter->user->role != "admin"
                                    ) {
                                        echo "<input type='hidden' name='" . $col["Field"] ."' value='" . $result[$col["Field"]] . "' required></input>";
                                } else {
                                        if ($edfields->type[$col["Type"]] === "textarea") {
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> <textarea cols='25' rows='9' title='" . $edfields->title[$col['Type']]  . "' placeholder='" . $edfields->place[$col['Type']]  . "' name='" . $col["Field"] ."'>" . $result[$col["Field"]] . "</textarea></li>";
                                        }
                                        else if ($edfields->type[$col["Type"]] === "checkbox") {
                                                
                                                $checked = ($result[$col["Field"]] === '1') ? "checked='checked'" : '';
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> <input type='" . $edfields->type[$col['Type']]  . "' title='" . $edfields->title[$col['Type']]  . "' placeholder='" . $edfields->place[$col['Type']]  . "' name='" . $col["Field"] ."' value='" . $result[$col["Field"]] . "' " . $checked . "></input></li>";
                                        } else {
                                                echo "<li><label>" . $this->table->displayCol($col["Field"]) . ":</label> <input type='" . $edfields->type[$col['Type']]  . "' title='" . $edfields->title[$col['Type']]  . "' placeholder='" . $edfields->place[$col['Type']]  . "' name='" . $col["Field"] ."' value='" . htmlspecialchars($result[$col["Field"]], ENT_QUOTES) . "'></input></li>";
                                        }
                                }
                        }
                }
                echo "
                </li><label> &nbsp;</label><input type='submit' value='save'></input></li>
                </ul>
                </form>";
                
    
        }
        
        function UpdateRow(){
                //firstly just set any checkbox type fields to 0. If they were not checked the form element does not exist.
                foreach ($this->table->columnInfo as $col) {
                        if ($col["Type"] === "tinyint(1)") {
                                if (!isset($this->filter->modpairs[$col["Field"]])) {
                                        $this->filter->AddModifyPair($col["Field"], '0');
                                }
                        }
                }

                $results = $this->dbConnect->SetRow($this->table, $this->filter);
        }
        
        function DeleteFormElements($rowid) {
                                echo ("
                      <div id='delete' class='clearfix'>
                      <p>Are you sure you wish to delete?</p>
                      
                      <form action='delete.php?t=" . $this->table->tablename . "&r=" . $rowid . "' method='POST'>
                      <input type='hidden' name='tablename' value='" . $this->table->tablename . "'></input>
                      <input type='hidden' name='rowid' value='" . $rowid . "'></input>
                      ");
                $results = $this->dbConnect->GetRow($this->table,$this->filter);
                
                foreach ($results as $result) {
                        foreach ($this->dbConnect->GetColumns($this->table->tablename) as $row) {
                                if (!(
                                    $this->table->primaryKey ===  $row["Field"] ||
                                    $this->table->isFKTo($row["Field"])
                                    )) {
                                        echo "<p><span class='dlabel'>" . $this->table->displayCol($row["Field"]) .": </span><span class='detail'>" . $result[$row["Field"]] . "</span></p>";
                                }
                        }
                }
                echo "
                <p><span class='dlabel'> &nbsp;</span><span class='detail'><input type='submit' value='DELETE'></input></span></p>
                </form>
                </div>";
        }
        
        function DeleteRow() {
                if ( !empty($_POST)) {
                        $results = $this->dbConnect->DeleteRow($this->table,$this->filter);
                }
        }
        
        function Nullify(&$listofvalues) {
                foreach($listofvalues as &$listitem) {
                        if ($listitem == NULL) { // == regards empty string as Null. We need to set empty string to actually be null, so that integer fields get updated to null instead of an empty string which gives an error.
                                $listitem = NULL;
                        }
                }
        }
        
}
?>