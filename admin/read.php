<?php
// Skelecred (c) William Pascoe, 2016
    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    include 'Filters.php';
    include 'Context.php';
    include 'Paging.php';

    $template = new Template();

    $user = $template->CheckAccess();

    $dbconn = new DBConn();

    $aTable = new TableDetails($_GET['t'], $dbconn);

    $aFilter = new Filters();

    $user->SetClause($aTable, $dbconn);

    $aFilter->AddUser($user);
    if (isset($_GET['r'])){
        $aFilter->AddFilterPair( $aTable->primaryKey, $_GET['r']);
    }
    else if (isset($_GET['f']) && isset($_GET['fk'])){
        // need to include tablename to avoid ambiguous references in SQL
        $aFilter->AddFilterPair( $aTable->tablename . "." . $_GET['f'], $_GET['fk']);
    }
    $cred = new Credify( $dbconn, $aTable, $aFilter );
    $template->Header($aTable->displayTablename());

    $context = new Context($dbconn);

    $context->show($aTable, $aFilter);

    echo "<p><a href='create.php?" . $_SERVER['QUERY_STRING'] . "'>Add</a></p>";

    if ($_SESSION["role"] === "admin") {
        echo("<p><a href='./read.php?t=" . $_GET["t"] . "'>Show All</a></p>");
    }

    echo '<table id="readtable" class="admin table">';
    $cred->AllColumns();

    $page = new Paging($dbconn->GetRecordCount($aTable, $aFilter));
    if( isset($_GET{'page'}) ) {
                 $page->ResetPage($_GET{'page'});
    }

    $cred->AllRows($page);

    echo '</table>';



    $page->PageControls($_SERVER['QUERY_STRING']);
    $template->Footer();
?>
