<?php

class Paging {

            public $page = 0;
            public $offset = 0;
            public $rec_limit = 10;
            public $rec_count = 0;
            
            public function __construct ($count) {
                $this->rec_count = $count;
            }
            
    function PageControls($qs) {
        $left_rec = $this->rec_count - ($this->page * $this->rec_limit);
        
        if( $this->page > 0 ) {
                $last = $this->page - 2;
                echo "<a href='" . $_SERVER['PHP_SELF'] . "?" . $this->NewQS($last) . "'>Last " . $this->rec_limit. " Records</a> |";
                echo "<a href='" . $_SERVER['PHP_SELF'] . "?" . $this->NewQS($this->page) . "'>Next " . $this->rec_limit. " Records</a>";
             }else if( $this->page == 0 ) {
                echo "<a href='" . $_SERVER['PHP_SELF'] . "?" . $this->NewQS($this->page) . "'>Next " . $this->rec_limit. " Records</a>";
             }else if( $this->left_rec < $this->rec_limit ) {
                $last = $this->page - 2;
                echo "<a href='" . $_SERVER['PHP_SELF'] . "?" . $this->NewQS($last) . "'>Last " . $this->rec_limit. " Records</a>";
             }
    }
    function ResetPage($pageno){
            $this->page = $pageno + 1;
            $this->offset = $this->rec_limit * $this->page;
         
    }
    function NewQS($pagestr) {
        parse_str($_SERVER['QUERY_STRING'], $query_string);
        $query_string['page'] = $pagestr;
        return http_build_query($query_string);
    }
}
?>