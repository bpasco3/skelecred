<?php
    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    include 'Filters.php';
    include 'Context.php';
    
    $template = new Template();
    
    $user = $template->CheckAccess();
        
    $dbconn = new DBConn();

    $aTable = new TableDetails($_GET['t'], $dbconn);

    $aFilter = new Filters();
    $user->SetClause($aTable, $dbconn);
    $aFilter->AddUser($user);
    
    $aFilter->AddFilterPair($aTable->primaryKey, isset($_GET['r']) ? $_GET['r'] : '');
        
    $crud = new Credify( $dbconn, $aTable, $aFilter );
    
    $context = new Context($dbconn);
    $subtitle = $context->getRowTitle($aTable, $aFilter);
    $template->Header($aTable->displayTablename() . ($subtitle ? ": " . $subtitle : ""));
    
    $context->show($aTable, $aFilter);
    echo "<div class='warnings'><p></p></div>";
    echo "<div id='details' class='clearfix'>";
    $crud->Details($_GET['r']);
    echo "</div>";
    
    $template->Footer();
?>
