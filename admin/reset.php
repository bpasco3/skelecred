<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
    include 'Credify.php';
    include 'DBConn.php';
    include 'Template.php';
    include 'access/AccountHandler.php';
    include 'Filters.php';
    require_once 'access/Validation.php';
    
    $template = new Template();
    $user = $template->CheckAccess();
    $dbconn = new DBConn();
    $aTable = new TableDetails("user", $dbconn);
    //$aFilter = new Filters();

    $template->Header("Change Password");

    $acc = new AccountHandler($dbconn);
    
    
    if (!isset($_POST["pw"])) {
        if (($user->role === "end") && ($user->iduser !== $_GET["un"])) {
            echo "<p class='warning'>Unable to reset another user's password.</p>";
            $template->Footer();
            exit;
        }
        $uFilter = new Filters();
        $uFilter->AddUser($user);
        $uFilter->AddModifyPair("iduser", $_GET["un"]);
        $acc->PResetForm($uFilter);
        
        
    } else {
        if ($_POST["pw"] !== $_POST["pw2"]) {
            echo "<p class='warnings'>Passwords do not match, please try again.</p>";
            $uFilter = new $uFilter();
            $uFilter->AddUser($user);
            $uFilter->AddModifyPair("iduser", $_POST["iduser"]);
            $acc->PResetForm($uFilter);
            $template->Footer();
            exit;
        } else {
            $aFilter = new Filters();
            $aFilter->AddUser($user);
            $pvalid = new VPassword("pw");
            $aFilter->AddValidator($pvalid);
            $validationResult = $aFilter->AddFormInputs($_POST);

            if ( $validationResult === '') {
                $asdf = $acc->PReset($aFilter);
                echo "<p>Password changed.</p>";
            } else {
                
                echo "<p class='warnings'>Error validating password.</p>";
            }
        }
    }
    
    
    
    $template->Footer();
?>