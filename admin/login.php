<?php

    include 'Template.php';
    include 'access/AccountHandler.php';
    include 'Filters.php';
    include 'DBConn.php';

    $template = new Template();
    $dbconn = new DBConn();
    
    $template->Header("Log In");
            
                if ( !empty($_POST) ) {
                    $aFilter = new Filters();
                    $aFilter->AddFormInputs($_POST);
                    $auth = new AccountHandler($dbconn);
                    $auth->Authenticate($aFilter);
                } else {
             echo "       
            <form action='login.php' method='post' class='clearfix'>
                <ul>
                <li><label>Username</label><input type='text' name='username'></input>
                </li><li><label>Password</label><input type='password' name='pw'></input>
                </li><li><input type='submit' value='log in'>
                </li>
                </ul>
            </form>
                        <p><a href='account.php'>Create Account</a></p>
            <p><a href='help.php'>Trouble Signing In?</a></p>
                ";
                }
            
            
    $template->Footer();
?>