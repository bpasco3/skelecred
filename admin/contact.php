<?php

    include 'Template.php';
    include 'DBConn.php';
    
    $template = new Template();
    $dbconn = new DBConn();
    $page = $dbconn->GetPage("Contacts");
    $template->Header($page[0]["title"]);
    echo $page[0]["content"];
    $template->Footer();
?>