<?php
    require_once 'access/SecurityClearance.php';
    require_once 'access/Validation.php';
// Skelecred (c) William Pascoe, 2016
class Filters {

    public $filterpairs = [];
    public $modpairs = [];
    public $formvalidations = [];
    private $security = NULL;
    public $user = NULL;

    public function __construct () {
        $this->security = new SecurityClearance();
        $vne = new VNotEmpty("*");
        $this->formvalidations[] = $vne;
    }
    
    public function AddValidator(Validation $valid){
        $this->formvalidations[] = $valid;
    }
    public function AddModifyPair($k, $v) {
        $this->CheckKey($k);
        $this->CheckValue($v);
        $this->modpairs[$k] = $v;

    }
    
    public function AddFilterPair($k, $v) {
        $this->CheckKey($k);
        $this->CheckValue($v);
        $this->filterpairs[$k] = $v;
        }

// will need to inject a validator made by a factory depending on the particular table.    
    public function AddFormInputs($form){

        foreach ($this->formvalidations as $validator) {
                foreach ($form as $k => $v) {
                    if ($validator->fieldName() === "*" || $validator->fieldName() === null || ($validator->fieldName() === $k)) {
                      //                      ";

                        if ($validator->isValid($v)) {
                            if (get_class($validator) === "VPassword") {

                                $v = password_hash($v,PASSWORD_DEFAULT);
                            }
                            $this->AddModifyPair($k, $v);
                        } else {
                            return $validator->failMessage($k);
                        }
                    } 
                }
        }
      //  echo "LOOP COMPLETE";
        return '';
    }

    public function PdoWhereAndFields() {
        $wfields = array_keys($this->filterpairs);
        foreach ($wfields as &$key) {
            $key = $key . " = ? ";
        }
        return implode(" AND ", $wfields);
    }
    public function PdoWhereAndUpdateFields() {
        $wfields = array_keys($this->filterpairs);
        unset($wfields['user.iduser']); // can't filter by user in the update statement
        if (($key = array_search('user.iduser', $wfields)) !== false) {
            unset($wfields[$key]);
        }
        
       // var_dump($wfields);
        foreach ($wfields as &$key) {
            $key = $key . " = ? ";
        }
        return implode(" AND ", $wfields);
    }
    public function PdoWhereOrFields() {
        $wfields = array_keys($this->filterpairs);
        foreach ($wfields as &$key) {
            $key = $key . " = ? ";
        }
        return implode(" AND ", $wfields);
    }
    
    public function PdoUpdateFields(){
        $mfields = array_keys($this->modpairs);
        foreach ($mfields as &$key) {
            $key = $key . " = ? ";
        }
        return implode(" , ", $mfields);
    }
    
    public function PdoFields() {
        $afields = array_keys($this->modpairs);

        return implode(",", $afields);
        
    }
    public function PdoValues() {
        $values = array_values($this->modpairs);
        $this->Nullify($values);
        return $values;
    }
    public function PdoWhereValues() {
        $values = array_values($this->filterpairs);
        $this->Nullify($values);
        return $values;
    }
    public function PdoWhereUpdateValue($pkey) {
        $value = array($this->filterpairs[$pkey]);
    //    var_dump($values);
        $this->Nullify($value);
        return $value;
    }
    public function PdoQMarks(){
        $afields = array_keys($this->modpairs);
        foreach ($afields as &$key) {
            $key = "?";
        }
        return implode(",", $afields);
    }
    
    public function CheckKey(&$k){
        // do something to check not empty, not already exist, strip spaces, no backtics, quotes etc.
        // Strip HTML
        $k = strip_tags($k);
        if(!$this->security->ColumnName($k)){
            exit;
        }
    }
    public function CheckValue(&$v){
        // do something to check not empty, not already exist, strip spaces, no backtics, quotes etc.
        // Strip HTML
        $v = strip_tags($v);
        
       if(!$this->security->ColumnName($v)){
            exit;
       }
    }
    // in general try to restrict access unless it is allowed, rather than allow access unless it is restricted.
    public function AddUser($user){
        
        if ($user->role !== "admin"){
            $this->AddFilterPair("user.iduser", $user->iduser);
        }
        $this->user = $user;
    }

        function Nullify(&$listofvalues) {
                foreach($listofvalues as &$listitem) {
                        if ($listitem == NULL) { // == regards empty string as Null. We need to set empty string to actually be NULL, so that integer fields get updated to null instead of an empty string which gives an error.
                                $listitem = NULL;
                        }
                }
        }
    
}
?>