<?php
// Skelecred (c) William Pascoe, 2016

    include 'Context.php';
    
class PublicContext extends Context {
    
    
    // overide to just not show the link to master.php
    function show($aTable, $aFilter) {
            
// make the filter a primary key filter.
                if ( ! isset($aFilter->filterpairs[$aTable->primaryKey])) { //
                    $rows = $this->dbconn->GetAllRows($aTable, $aFilter, NULL);
                    if (count($rows) > 0) {
                        $singleRow = $rows[0];
                        $aFilter = new Filters();
                        $aFilter->AddUser($this->dummy);
                        $aFilter->AddFilterPair($aTable->primaryKey, $singleRow[$aTable->primaryKey]);
                    }
                    
                }

                $this->recurseUp($aTable, $aFilter);
                array_unshift($this->contextList," <a href='/'>Home</a>");
                echo "<div class='context'>";
                echo join(" > ", $this->contextList);
                echo "</div>";
            }

            function recurseUp($pkTable, $pkFilter) {
            
            // get the referenced table, the referenced table key and the referenced table value
            // get the referenced table record and it's 'title' 
        
            // so the passed in filter should be a pkey = id.
            
            $fks = $pkTable->foreignKeys;
            $rows = $this->dbconn->GetAllRows($pkTable, $pkFilter, NULL);

                if (empty($fks) || empty($pkFilter->filterpairs) || (count($pkFilter->filterpairs) === 1 && isset($pkFilter->filterpairs["user.iduser"]))){
                        array_unshift($this->contextList, " <a href='index.php?t=" . $pkTable->tablename . "'>" .  $pkTable->displayTablename() . "</a>");
                } else if (count($rows) < 1) {
                            $nTable = null;

                            foreach ($fks as $fka) {
                                
                                
                                
                             //   echo "" . $pkTable->tablename . "." . $fka["COLUMN_NAME"];
                                    if (isset($pkFilter->filterpairs[$pkTable->tablename . "." . $fka["COLUMN_NAME"]])) {
                                        $nTable = new TableDetails($fka["REFERENCED_TABLE_NAME"], $this->dbconn);
                                        $startFilter = new Filters();
                                        $startFilter->AddUser($this->dummy);
                                        $startFilter->AddFilterPair($fka["REFERENCED_COLUMN_NAME"], $pkFilter->filterpairs[$pkTable->tablename . "." . $fka["COLUMN_NAME"]]);
                                    }

                            }

                            $refTitle = $this->getRowTitle($nTable, $startFilter);

                            array_unshift($this->contextList, " <a href='read.php?t=" . $nTable->tablename .
                                          "&r=" . $pkFilter->filterpairs[$pkTable->tablename . "." . $fka["COLUMN_NAME"]] .
                                          "'>" . $nTable->displayTablename() . ": " .
                                          $refTitle . "</a>");
                            

                            $this->recurseUp($nTable, $startFilter);
                        

                }
                else {
                    foreach ($fks as $fk) {
                        $singleRow = $rows[0]; //should only be one since we filtered by primary key.
                        $refTable = new TableDetails($fk["REFERENCED_TABLE_NAME"], $this->dbconn);
                        $refFilter = new Filters();
                        $refFilter->AddUser($this->dummy);
                        $refFilter->AddFilterPair($fk["REFERENCED_COLUMN_NAME"], $singleRow[$fk["COLUMN_NAME"]]);
                        
                        $refTitle = $this->getRowTitle($refTable, $refFilter);
                        
                        array_unshift($this->contextList, " <a href='index.php?t=" . $refTable->tablename . "&r=" . $singleRow[$fk["COLUMN_NAME"]] . "'>" . $refTable->displayTablename() . ": " . $refTitle . "</a>");

                        $this->recurseUp($refTable, $refFilter);
                        
                    }
               }
            }
            
}
?>