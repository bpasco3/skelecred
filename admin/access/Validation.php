<?php

// Validation should be done at front end with javascript/jquery
// But it must also be done at server side as it is easy to bypass javascript validation accidentally (user disables javascript, or some browser incompatibility) or on purpose.

// declare an interface so we could create lists of validations to loop through and check
// Maybe make it all static. Maybe use late static binding.
interface Validation {
    public function fieldName(); // return * if applies to all fields.
    public function isValid($check);
    public function failMessage($field);
}

class VDummy implements Validation {
	    private $fieldname;

    public function __construct ($field) {
        $this->fieldname = $field;
    }
    public function fieldName() {
        return $this->fieldname;
    }
    public function isValid($check) {
		return TRUE;
	}
	public function failMessage($field) {
        return "<p>This dummy validation should always return ok.</p>";
    }
}

class VNotEmpty implements Validation {
    private $fieldname;

    public function __construct ($field) {
        $this->fieldname = $field;
    }

    public function fieldName() {
        return $this->fieldname;
    }

    public function isValid($check) {
        return ($check != null);
    }

    public function failMessage($field){
        return "An input value is required for " . $field;
    }
}

class VPassword implements Validation {
    private $fieldname;

    public function __construct ($field) {
        $this->fieldname = $field;
    }
    public function fieldName() {
                return $this->fieldname;
    }
    public function isValid($check) {
    	if (!preg_match("/[a-z]/", $check))
    	{
    		return FALSE;
    	}
    	else if (!preg_match("/[A-Z]/", $check))
    	{
    		return FALSE;
    	}
    	else if (!preg_match("/[0-9]/", $check))
    	{
    		return FALSE;
    	}
		else if (!preg_match("/[^a-zA-Z0-9]/", $check))
    	{
    		return FALSE;
    	}
		else if (strlen($check) < 8)
    	{
    		return FALSE;
    	}
    	else
    	{
    		return TRUE;
    	}
    }
    public function failMessage($field) {
        return "<p>Password Invalid.</p>";
    }
}

class VUniqueUsername implements Validation {
    private $fieldname;

    public function __construct ($field) {
        $this->fieldname = $field;
    }
    public function fieldName() {
        return $this->fieldname;
    }
    public function isValid($check) {
        return TRUE;
    }
    public function failMessage($field) {
        return "Username not available. Please try another.";
    }
}

?>