<?php

class User {
    
    public $iduser = '';
    public $username = '';
    public $role = '';
    private $filterClause = '';
    
    
        public function __construct ($useridin, $usernamein, $rolein) {
            $this->iduser = $useridin;
            $this->username = $usernamein;
            $this->role = $rolein;
        }
        public function SetClause($aTable, &$dbConn) {
            $this->filterClause = '';
            $this->recurseClause($aTable, $dbConn);


        }
        public function recurseClause($aTable, &$dbConn){
            $fks = $aTable->foreignKeys;
            if ($fks) {
                foreach ($fks as $fka) {
                    $this->filterClause = $this->filterClause . " JOIN " . $fka["REFERENCED_TABLE_NAME"] . " ON " . $fka["TABLE_NAME"] . "." . $fka["COLUMN_NAME"] . " = " . $fka["REFERENCED_TABLE_NAME"] . "." . $fka["REFERENCED_COLUMN_NAME"] . " ";
                }
    
                if ($fka["REFERENCED_TABLE_NAME"] != "user") {
                    $nTable = new TableDetails($fka["REFERENCED_TABLE_NAME"], $dbConn);
                    $this->recurseClause($nTable, $dbConn);
                }
            }
        }
        
        public function GetClause() {
            if ($this->role !== "admin") {
                return $this->filterClause;
            } else {
                return " ";
            }
        }
      
} 







?>