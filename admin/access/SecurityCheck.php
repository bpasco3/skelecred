<?php

// these are assumed to be regex checks, do something else for other kinds of checks.
class SecurityCheck {
    public $pass = FALSE;
    private $pattern = NULL;
    public $failMessage = "Security Error.";
    
        public function __construct ($regex, $failmessage) {
            $this->pattern = $regex;
            $this->failMessage = $failmessage;
        }
        public function Check($suspect) {
            $pass = !preg_match($this->pattern, $suspect);
            return $pass;
        }
        

}



?>