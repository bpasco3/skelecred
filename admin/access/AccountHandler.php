<?php

class AccountHandler {
    public $dbConnect;
    
    public function __construct ($dbin) {
        $this->dbConnect = $dbin;
    }
    
    public function UsernameExists($aFilter) {
        $auth = $this->dbConnect->UserExists($aFilter);
        return count($auth) > 0;
    }
    
    function Authenticate($aFilter) {
        $auth = $this->dbConnect->GetUser($aFilter);

        if ($auth == null) {
            echo "<p class='warnings'>Unable to authenticate. Incorrect username or password.</p>";
        } else if (password_verify($aFilter->modpairs["pw"], $auth[0]["pw"])) {
            echo "<p>Logged in. <a href='index.php'>Home</a></p>";
            // Check role
            // Set some sessions and, later, give a link to:
            // edit account details
            $_SESSION["iduser"] = $auth[0]["iduser"];
            $_SESSION["user"] = $auth[0]["username"];
            $_SESSION["authentic"] = "1";
            $_SESSION["role"] = $auth[0]["role"];
            $_SESSION["displayname"] = $auth[0]["displayname"];
        } else {
            echo "<p class='warnings'>Unable to authenticate.</p>";
        }

    }
    function UsernameForm(){
        
        echo "
            <form action='account.php' method='post' class='clearfix'>
                <ul class='clearfix'>
                <li><label>Username</label><input type='text' name='username'></input>
                </li>
                </ul>

                <p>To help prevent robot attacks please choose the animal pictured:</p>
                <img id='animal' src='' />
                <ul class='clearfix' style='float: left;'>
                <li><label>Cat</label><input type='radio' name='animal' value='453'></radio>
                </li><li><label>Deer</label><input type='radio' name='animal' value='828'></radio>
                </li><li><label>Dolphin</label><input type='radio' name='animal' value='246'></radio>
                </li><li><label>Elephant</label><input type='radio' name='animal' value='345'></radio>
                </li><li><label>Hippopotamus</label><input type='radio' name='animal' value='714'></radio>
                </li><li><label>Kangaroo</label><input type='radio' name='animal' value='783'></radio>
                </li><li><label>Koala</label><input type='radio' name='animal' value='393'></radio>
                </li><li><label>Llama</label><input type='radio' name='animal' value='902'></radio>
                </li><li><label>Mouse</label><input type='radio' name='animal' value='785'></radio>
                </li><li><label>Owl</label><input type='radio' name='animal' value='127'></radio>
                </li><li><label>Pigeon</label><input type='radio' name='animal' value='828'></radio>
                </li><li><label>Racoon</label><input type='radio' name='animal' value='839'></radio>
                </li><li><label>Wolf</label><input type='radio' name='animal' value='634'></radio>
                </li><li><input type='hidden' id='animalcode' name='animalcode' value='323'></input><input type='submit' value='Create'></input>
                </li>
                </ul>
    
                </form>
                ";
    }
    
    function CreateAccountForm ($un) {
        echo "
            <form action='account.php' method='post' class='clearfix'>
                <ul>
                <li><label>Username</label><input type='hidden' name='username' value=" . $un . "></input>" . $un . "
                <input type='hidden' name='active' value='1'></input>
                </li><li><label>Display Name</label><input type='text' name='displayname'></input>
                </li><li><label>Country</label><input type='text' name='country'></input>
                </li><li><label>Institute</label><input type='text' name='institute'></input>
                </li><li><label>Department</label><input type='text' name='department'></input>
                </li><li><label>Department Head</label><input type='text' name='departmenthead'></input>
                </li><li><label>Group Leader</label><input type='text' name='groupleader'></input>
                </li><li><label>Researcher</label><input type='text' name='researcher'></input>
                </li><li><label>Email</label><input type='text' name='email'></input>
                </li><li><label>Phone</label><input type='text' name='phone'></input>
                </li><li><label>Mobile</label><input type='text' name='mobile'></input>
                </li><li><label>Password</label><input type='password' name='pw'></input>
                </li><li><input type='submit' value='Create'>
                </li>
                </ul>
            </form>
            <h3>Password Requirements</h3>
            <ol>
            <li>Passwords must be between 8 and 16 characters (inclusive) in length.
            </li><li>Passwords must include at least one uppercase and one lowercase letter, one number and one special character (i.e. %,!, * or similar).
            </li><li>Passwords must not contain any user ID, student ID, or any part of the user’s name.
            </li>
            </ol>
        ";
    }
    
    function PReset($aFilter) {
        return $this->dbConnect->SetP($aFilter);
    }
    
    function PResetForm($aFilter) {
        $userdetail = $this->dbConnect->GetUserById($aFilter);
        $userdet = $userdetail[0];
        echo "
            <form action='reset.php' method='post' class='clearfix'>
                <ul>
                <li><label>Username</label><input type='hidden' name='iduser' value=" . $userdet['iduser'] . "></input>" . $userdet['username'] . "
                </li><li><label>Password</label><input type='password' name='pw'></input>
                </li><li><label>Retype Password</label><input type='password' name='pw2'></input>
                </li><li><input type='submit' value='Change'>
                </li>
                </ul>
            </form>
            <h3>Password Requirements</h3>
            <ol>
            <li>Passwords must be between 8 and 16 characters (inclusive) in length.
            </li><li>Passwords must include at least one uppercase and one lowercase letter, one number and one special character (i.e. %,!, * or similar).
            </li><li>Passwords must not contain any user ID, student ID, or any part of the user’s name.
            </li>
            </ol>
        ";
    }


}

?>