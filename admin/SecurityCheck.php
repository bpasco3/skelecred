<?php

class SecurityCheck {
    public $pass = FALSE;
    private $pattern = NULL;
    public $failMessage = "Security Error.";
    
        public function __construct ($regex, $failmessage) {
            $this->pattern = $regex;
            $this->failMessage = $failmessage;
        }
        public function Check($suspect) {
            $pass = !preg_match($this->pattern, $suspect);
            return $pass;
        }
        

}



?>