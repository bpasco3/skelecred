<?php
// Skelecred (c) William Pascoe, 2016
class DBConnCore {

    private $host = "clockworksparrow.com";
    private $dbname = "test";
    private $un = "root";
    private $pw = "rootpass";

    public $storedb = NULL;

    public $validTables = ''; // this could be modified to not be created dynamically, but a fixed list, if you want to restrict access by specifying what tables.

    // detect if we're on dev or live and connects to relevant db
    function GetDB() {
        if ($this->storedb == NULL) {
            if ($_SERVER["SERVER_NAME"] == "localhost") {
                $this->host = "localhost";
                $this->dbname = "test";
                $this->un = "root";
                $this->pw = "rootpass";
            }
            $this->storedb = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->un, $this->pw);
            return $this->storedb;
        } else {
            return $this->storedb;
        }
    }


    function GetTables() {
        $db = $this->GetDB()->prepare("show tables"); // may need to be different for type of db.
        $db->execute();
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }

    function GetColumns($tablename) {

        $db = $this->GetDB()->prepare("DESCRIBE ". $tablename);
        $db->execute();
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }

    // filter should be an array of key value pairs that will be joined as an AND
    function GetAllRows($aTable, $filter, $paging) {

     $fields = implode($aTable->GetColumnNames(), ",");
     $userFilter = isset($filter->user) ? $filter->user->GetClause() : "";     
     
        $pageClause = '';
        if ($paging != NULL){
            $pageClause = " LIMIT " . $paging->offset . ", " . $paging->rec_limit;
        }
        if (($filter == NULL) || ($filter->PdoWhereAndFields() == NULL)) {
            $db = $this->GetDB()->prepare("SELECT " . $fields . " FROM " . $aTable->tablename . " " . $userFilter . " " . $pageClause);
            $db->execute();
            return $this->XSSClean($db->fetchAll(PDO::FETCH_ASSOC));
        } else {
            //$whereClause = implode(' AND ', array_map(function ($v, $k) { return $k . ' = ' . $v; }, $filter, array_keys($filter)));

            $db = $this->GetDB()->prepare("SELECT " . $fields . " FROM " . $aTable->tablename . " "  . $userFilter . " WHERE " . $filter->PdoWhereAndFields() . $pageClause);
            $db->execute($filter->PdoWhereValues());
            #var_dump($db->fetchAll(PDO::FETCH_ASSOC));
            return $this->XSSClean($db->fetchAll(PDO::FETCH_ASSOC));
        }
    }

    function GetRecordCount($aTable, $filter){
        $db = $this->GetDB()->prepare("SELECT count(" . $aTable->primaryKey . ") FROM " . $aTable->tablename);
        if (($filter == NULL) || ($filter->PdoWhereAndFields() == NULL)) {
            $db = $this->GetDB()->prepare("SELECT count(" . $aTable->primaryKey . ") FROM " . $aTable->tablename );
            $db->execute();
            //return $db->fetchAll(PDO::FETCH_ASSOC);
            return $db->fetchColumn();
        } else {
            //$whereClause = implode(' AND ', array_map(function ($v, $k) { return $k . ' = ' . $v; }, $filter, array_keys($filter)));
            $db = $this->GetDB()->prepare("SELECT count(" . $aTable->primaryKey . ") FROM " . $aTable->tablename . " WHERE " . $filter->PdoWhereAndFields() );
            $db->execute($filter->PdoWhereValues());
                        //return $db->fetchAll(PDO::FETCH_ASSOC);
            return $db->fetchColumn();
        }
    }

    function GetRow($aTable, $filter) {
        $fields = implode($aTable->GetColumnNames(), ",");
        $userFilter = isset($filter->user) ? $filter->user->GetClause() : "";
        $db = $this->GetDB()->prepare("SELECT " . $fields . " FROM " . $aTable->tablename . " " . $userFilter . " WHERE " . $filter->PdoWhereAndFields() );
        $db->execute($filter->PdoWhereValues());
        return $this->XSSClean($db->fetchAll(PDO::FETCH_ASSOC));
        /*
        $db = $this->GetDB()->prepare("SELECT * FROM ".$tablename." WHERE " . $this->GetTablePKey($tablename) . " = " . $rowid );
        $db->execute();
        return $db->fetchAll(PDO::FETCH_ASSOC);
        */
    }

    function InsertRow($aTable, $filter) {
        try {

            // assumes auto-increment primary key
          //  echo("INSERT INTO " . $aTable->tablename . " ("  . $filter->PdoFields() . ") VALUES (" . $filter->PdoQMarks() . ")");
          //  echo implode(",",$filter->PdoValues());

           $this->GetDB()->setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);

//echo "INSERT INTO " . $aTable->tablename . " ("  . $filter->PdoFields() . ") VALUES (" . $filter->PdoQMarks() . ")";
//echo var_dump($filter->PdoValues());
            $db = $this->GetDB()->prepare("INSERT INTO " . $aTable->tablename . " ("  . $filter->PdoFields() . ") VALUES (" . $filter->PdoQMarks() . ")");
            $result = $db->execute($filter->PdoValues());
            $newrowid = $this->GetDB()->lastInsertId();

            return $newrowid;
        }
        catch(PDOException $e)
        {
            echo $sql . "<p class='error'>" . $e->getMessage() . "</p>";
        }
    }

    function SetRow($aTable, $filter){
        try {
            //echo "
            //UPDATE " . $aTable->tablename ." SET " . $filter->PdoUpdateFields() . " WHERE " . $filter->PdoWhereAndUpdateFields();
            //var_dump(array_merge($filter->PdoValues(), $filter->PdoWhereValues()));
            $db = $this->GetDB()->prepare("UPDATE " . $aTable->tablename ." SET " . $filter->PdoUpdateFields() . " WHERE " . $aTable->primaryKey . " = ?" );

            //$db = $this->GetDB()->prepare("UPDATE " . $aTable->tablename ." SET " . $filter->PdoUpdateFields() . " WHERE " . $filter->PdoWhereAndUpdateFields() );

            //$result = $db->execute(array_merge($filter->PdoValues(), $filter->PdoWhereValues()));

            $result = $db->execute(array_merge($filter->PdoValues(), $filter->PdoWhereUpdateValue($aTable->primaryKey)));
            if ($db->rowCount()) {
                echo "<p class='result'>Result:" . $db->rowCount() . " records updated.</p>";
            } else {
                echo "<p class='error'>ERROR: " . $db->rowCount() . " records updated.</p>";
            }
        }
        catch(PDOException $e)
        {
            echo $sql . "<p class='error'>" . $e->getMessage() . "</p>";
        }
    }

    function DeleteRow($aTable, $filter){
        try {

        $db = $this->GetDB()->prepare("DELETE FROM " . $aTable->tablename . " WHERE " . $aTable->primaryKey . " = ?" );
        $result = $db->execute($filter->PdoWhereUpdateValue($aTable->primaryKey));
            if ($db->rowCount()) {
                echo "<p class='result'>RESULT " . $db->rowCount() . " records deleted.</p>";
            } else {
                echo "<p class='warnings'>ERROR: " . $db->rowCount() . " records deleted.</p><p class='warnings'>You may need to delete other data connected to this record before attempting to delete this record.</p><p class='warnings'>This is to prevent data being 'orphaned', where it exists, but becomes inaccessible because it is not connected to anything.</p>";
            }
        }
        catch(PDOException $e)
        {
            echo $sql . "<p class='error'>" . $e->getMessage() . "</p>";
        }
    }

    // UTILITY FUNCTIONS
    function GetTablePKey($aTable) {
        foreach ($this->GetColumns($aTable) as $row) {
                if ($row["Key"] === "PRI") {
                        return $row["Field"];
                }
        }
       return null;
    }
    /*function GetForeignKeys ($aTable){
                foreach ($this->GetForeignKeyCols($aTable) as $row) {
                    foreach ($row as $field) {

                        echo "<p> " . $row . "~~~~~ " . $field . "</p>";
                    }

                }
    }*/

    function GetForeignKeyCols($tablename) {

        try {
            $db = $this->GetDB()->prepare("SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = ? AND REFERENCED_TABLE_NAME IS NOT NULL" );
            $result = $db->execute(array($tablename));
            if ($db->rowCount()) {
                return $db->fetchAll(PDO::FETCH_ASSOC);
            } else {
                return $db->rowCount();
            }
        }
        catch(PDOException $e)
        {
            echo $sql . "<p class='error'>" . $e->getMessage() . "</p>";
        }
    }

    function GetTablesLinkedTo($tablename) {
        try {

            $db = $this->GetDB()->prepare("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_SCHEMA = '" . $this->dbname . "' AND REFERENCED_TABLE_NAME = ?" );
            $result = $db->execute(array($tablename));
            if ($db->rowCount()) {
                return $db->fetchAll(PDO::FETCH_ASSOC);
            } else {

                return $db->rowCount();
            }
        }
        catch(PDOException $e)
        {
            echo $sql . "<p class='error'>" . $e->getMessage() . "</p>";
        }
    }

    function GetTest(){
        $db = $this->GetDB()->prepare("SELECT * FROM just make a basic query from a table to test connection is ok.");
        $db->execute();
        return $db->fetchAll();
    }

    // html encodes everything to avoid XSS attacks
    function XSSClean($results) {
        foreach ($results as &$row) {
                foreach ($row as $key => &$field) {
                    $field = htmlspecialchars($field);
                }
        }
        return $results;
    }

}

?>