$(document).ready(function() {






// set checkbox value to 0 or 1 for tinyint(1) mysql input
    $('input:checkbox').change(function() {
        if(this.checked) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });


    
// 
        $("div#content form").submit(function (e) {
            $("div#content form :input").removeClass("warn");
            
        // validate password
            var pwproblems = [];
            var pw = $(":password").val();
            if (pw.length < 8 || pw.length > 18) {
                pwproblems.push("Must be at least 8 and at most 16 characters long.");
            }
            if (pw) {
                
                var regChecks = new Object();
                regChecks['[A-Z]'] = "Must contain upper case.";
                regChecks['[a-z]'] = "Must contain lower case.";
                regChecks['[0-9]'] = "Must contain a number.";
                regChecks['[^A-Za-z0-9_]'] = "Must contain a special character (eg: ~#$_.)";
                for (var k in regChecks) {
                    if(!(pw.match(k))) {
                        pwproblems.push(regChecks[k]);
                        $(":password").addClass("warn");
                    }
                }
                
            }
            
        
        // no empty values

            var emptyvalues = [];
            $("div#content form :input").each(function() {

               // $values[this.name] = $(this).val();     
                if (!$(this).val()) {
                   // alert("nooioi ");
                   // alert("8" + $(this).val());
                   $(this).addClass("warn");
                    emptyvalues.push($(this).val());
                }
            });
                      //  alert("2");
                      
            if (emptyvalues.length == 0 && pwproblems.length == 0) {
                return true;
            } else {
                e.preventDefault();
                $errorReport = '';
                if (pwproblems.length != 0) {
                    $errorReport = " <p>Password insecure:</p><ul><li>" + pwproblems.join("</li><li>") + "</li></ul>";
                    //$("div.warnings p").html(" <p>" + pwproblems.join("</p><p>") + "</p>");
                }
                if (emptyvalues.length != 0) {
                    $errorReport = $errorReport + "<p>All fields must be completed. If there is no information use 'NA'.</p>";
                }
               
                $("div.warnings p").html($errorReport);
              // $("div.warnings").addClass("warn");
                $("div.warnings").show();

                return false;
            }
        });

        
        $('a#printable').click(function(e){
            e.preventDefault();
            if ($('link#mainstyle').attr('href') == './printable.css') {
                $('link#mainstyle').attr('href','./skelecred.css');
            } else {
                $('link#mainstyle').attr('href','./printable.css');
            }
            $('div.stepdetails').show();
        });
        
        
        // no robot
var animalimage = new Array ();
animalimage["./img/256.jpg"] = 453;
animalimage["./img/585.jpg"] = 828;
animalimage["./img/319.jpg"] = 246;
animalimage["./img/426.jpg"] = 345;
animalimage["./img/902.jpg"] = 714;
animalimage["./img/178.jpg"] = 783;
animalimage["./img/553.jpg"] = 393;
animalimage["./img/428.jpg"] = 902;
animalimage["./img/954.jpg"] = 785;
animalimage["./img/250.jpg"] = 127;
animalimage["./img/732.jpg"] = 828;
animalimage["./img/162.jpg"] = 839;
animalimage["./img/957.jpg"] = 634;

  var keys = Object.keys(animalimage);
  var animalkey = keys[Math.floor(keys.length * Math.random())];

$('img#animal').attr('src',animalkey);
$('input#animalcode').val(animalimage[animalkey]*13);
    
});