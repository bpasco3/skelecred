<?php

// Skelecred (c) William Pascoe, 2016
    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    
    $template = new Template();
    $template->Header("Help");
    
//    ini_set('default_charset', 'UTF-8');
//    ini_set('display_errors', 1);
//    error_reporting(E_ALL);
    $dbconn = new DBConn();
    $crud = new Custom( $dbconn, NULL, NULL );
    $crud->GetHelp();

    $template->Footer();

?>
