<?php
    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    
    $template = new Template();
    $dbconn = new DBConn();
    $crud = new Credify( $dbconn );
    
    $template->Header();
    
    echo "<p><a href='create.php?" . $_SERVER['QUERY_STRING'] . "'>Add</a></p>";

    echo '<table id="readtable" class="admin table">';
    $crud->AllColumns($_GET["t"]);
    //isset($_GET['f']) ? $_GET['f'] : ''

    $crud->AllRows($_GET["t"],isset($_GET['f']) ? $_GET['f'] : '',isset($_GET['fk']) ? $_GET['fk'] : '');
    echo '</table>';
    
    //list all details of columns
   // $crud->ColDetails($_GET["t"]);
    
    $template->Footer();
?>