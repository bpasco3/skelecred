<?php

require_once 'SecurityCheck.php';
// Note that these are some basic checks for obvious weaknesses only. You remain responsible for maintaining your own security. You may wish to add to or modify this.
// making it so the functions that directly query the db by constructing SQL statements use only the Table and
// Filter objects as input, rather than raw strings means we can do all our checking in one place (in the table
// and filter objects) as a extra precaution to PDO.
class SecurityClearance {
    public $checks = [];
    
    public function __construct () {
        // the associative array is like a factory built as a hash.
        $this->checks["backticks"] = new SecurityCheck("/^[`]/i","No backticks. No SQL injection please. Logging IP:" . $_SERVER['REMOTE_ADDR'] . " Legal or preventative action may be pursued if this goes on.");
        $this->checks["alphanum"] = new SecurityCheck("/[^a-zA-Z0-9\s_-]/i","Security condition failed. Alphanumeric only."); //allows underscore and hyphen
    }
    function Table($tablename) {
        return $this->AllChecks($tablename);
    }
    function ColumnName($colname) {
        return $this->AllChecks($colname);
    }
    // I think PDO should handle most risks, and we want to allow characters that pdo would escape for us, so for inputs, lets just catch backticks as a double precaution
    function InputValue($inputval){
            if ($this->checks["backticks"]->Check($inputval)) {
                 return TRUE; 
            } else {
                echo "<p>" . $this->checks["backticks"]->failMessage . "</p>";
                return TRUE;
            }
    }
    function AllChecks($suspect){
        foreach ($this->checks as $key => $value) {
            if ($value->Check($suspect)) {
                 return TRUE; 
            } else {
                echo "<p>" . $value->failMessage . "</p>";
                return TRUE;
            }
        }
    }

}

?>