<?php
    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    include 'Filters.php';
    include 'Context.php';
    
    $template = new Template();
    
    $user = $template->CheckAccess();
        
    $dbconn = new DBConn();
        
    $aTable = new TableDetails($_GET['t'], $dbconn);

    $aFilter = new Filters();
    $user->SetClause($aTable, $dbconn);
    $aFilter->AddUser($user);

    $aFilter->AddFilterPair($aTable->primaryKey, isset($_GET['r']) ? $_GET['r'] : '');
    
    $crud = new Credify( $dbconn, $aTable, $aFilter );
    
    $context = new Context($dbconn);
    $subtitle = $context->getRowTitle($aTable, $aFilter);
    $template->Header($aTable->displayTablename() . ($subtitle ? ": " . $subtitle : ""));
    
    $context->show($aTable, $aFilter);
    
// use the post to avoid deleting without certainty warning.
    if ( !empty($_POST)) {
        $crud->DeleteRow();
    } else {
        $crud->DeleteFormElements($_GET['r']);
    }

    
    
    $template->Footer();
?>
