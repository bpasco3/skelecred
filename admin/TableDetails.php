<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

    include 'access/SecurityClearance.php';
    include_once 'Display.php';
class TableDetails
{
    // property declaration
    public $tablename = '';
    public $dbConnect = NULL;
    public $primaryKey = NULL;
    public $foreignKeys = '';
    public $foreignRelations = '';
    public $columnInfo = NULL;
    public $display = NULL;
    public $defaultvals = NULL;
    public $hidevals = NULL;
    public $displayTablename = ''; // this may be redundant, check and delete, or maybe switch to using this instead of the method below.
    public $titlefield = "";

    public function __construct ($tablenamein, &$dbin) {
        // construct it all at once, investing the expense of building it all at once when we might not need it, to re-use the table object instead of repeated calls to DB which would be clumsier.
        $security = new SecurityClearance();
        $aDisplay = new Display();
        
        if ($tablenamein == null && $aDisplay->defaultTable != null) {
            $tablenamein = $aDisplay->defaultTable;
        }
        
        if (isset($this->display->m[$this->tablename])){
            //get only the details relevant to this table for convenience
            $this->display = $this->aDisplay->m[$this->tablename];            
        }

        if ($security->Table( $tablenamein )){
            
            $this->tablename = $tablenamein;
            $this->dbConnect = $dbin;
            $this->display = isset($aDisplay->m[$tablenamein])? $aDisplay->m[$tablenamein] : NULL;
            $this->defaultvals = isset($aDisplay->def[$tablenamein])? $aDisplay->def[$tablenamein] : NULL;
            $this->hidevals = isset($aDisplay->hide[$tablenamein])? $aDisplay->hide[$tablenamein] : NULL;
            $this->meta = isset($aDisplay->table[$tablenamein])? $aDisplay->table[$tablenamein] : NULL;
            // set up to call DB once instead of many times.
            // this would be an appropriate place to validate and restrict access to tables.

            $this->fetchColumns();

            $this->fetchPrimaryKey();
    
            $this->fetchForeignKeys();
            $this->fetchForeignRelations();
        }
    }
    
    public function displayTablename(){
        return isset($this->meta["tablename"]) ? $this->meta["tablename"] : $this->tablename;
    }
    public function displayTitle() {
        return isset($this->meta["titlefield"]) ? $this->meta["titlefield"] : "";
    }
    // limit fields to show when listing results in a table, as opposed to the full details of a row. We might want to only show a few key fields to avoid a massively wide table on the page.
    // Because we need it to work magically without configuration, we desplay field be default
    public function hideInList($colname) {
        if (isset($this->hidevals)) {
            return in_array($colname, $this->hidevals);
        } else {
            return 0;
        }
    }
    public function displayCol($colname){
        return isset($this->display[$colname]) ? $this->display[$colname] : $colname;
    }
    public function defaultVal($colname){
        return isset($this->defaultvals[$colname]) ? $this->defaultvals[$colname] : "";
    }

    
    private function fetchColumns() {
        
        $this->columnInfo = $this->dbConnect->GetColumns($this->tablename);

        if ($this->columnInfo == NULL){
            echo "<p>Invalid table. Logging IP:" . $_SERVER['REMOTE_ADDR'] . " Legal or preventative action may be pursued if this goes on.</p>";
            error_log("Skelecrud error. Could not fetch columns with table name, " . $this->tablename . " from IP: " . $_SERVER['REMOTE_ADDR'], 0);
         //   error_log("Skelecrud error. Failed attempt to create table object with non-existent table name, " . $this->tablename , " from IP: " . $_SERVER['REMOTE_ADDR'], 1,
         //      "youraddress@somewhere.com");
            exit();
        }
    }
    
    private function fetchPrimaryKey(){
       foreach ($this->columnInfo as $row) {
               if ($row["Key"] === "PRI") {
                      $this->primaryKey = $row["Field"];
                      return;
                }
       }
    }
    
    private function fetchForeignKeys() {
        $this->foreignKeys = $this->dbConnect->GetForeignKeyCols($this->tablename);
    }
    
    private function fetchForeignRelations(){
        $this->foreignRelations = $this->dbConnect->GetTablesLinkedTo($this->tablename);
    }
    
    public function IsFKTo($field){

        if ($this->foreignKeys) {
                foreach ($this->foreignKeys as $row) {
                    if ($row["COLUMN_NAME"] === $field){
                        return Array($row['REFERENCED_TABLE_NAME'] => $row['REFERENCED_COLUMN_NAME']);
                    }
                }
        }
        return NULL;
    }
    // The inverse of IsFK, for where this table and this column is linked to by FK in a different table.
    // You need to be able to view and create records in that table attached to this particular row.
    public function IsFKFrom($field){

        $foreignTables = [];
        if ($this->foreignRelations == NULL) {
            return NULL;
        }
                foreach ($this->foreignRelations as $row) {
                    if ($row['REFERENCED_COLUMN_NAME'] === $field) {
                        $foreignTables[] = Array($row['TABLE_NAME'] => $row['COLUMN_NAME']);
                    }
                }
        return $foreignTables;
    }
    public function ColumnDescription($colname){
        foreach ($this->columnInfo as $row) {
        }
    }
    public function GetColumnNames(){
        $fnames = array_column($this->columnInfo, 'Field');
    foreach ($fnames as &$fn)
        $fn = $this->tablename . "." . $fn;
    return $fnames;
    }
}
?>