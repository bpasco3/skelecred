<?php

    include 'Credify.php';
    include 'Template.php';
    include 'DBConn.php';
    include 'Filters.php';
    include 'Context.php';
    
    $template = new Template();
    
    $user = $template->CheckAccess();
    $dbconn = new DBConn();
    
    $aTable = new TableDetails($_GET["t"], $dbconn);
    $aFilter = new Filters();

    $user->SetClause($aTable, $dbconn);
    $aFilter->AddUser($user);

    $validationResult = null;
    if ( !empty($_POST)) {
        $validationResult = $aFilter->AddFormInputs($_POST);
    }

    if (isset($_GET['r'])){
        $aFilter->AddFilterPair( $aTable->primaryKey, $_GET['r']);
    } else if (isset($_GET["f"])){
         $aFilter->AddFilterPair($aTable->tablename . "." . $_GET['f'], $_GET['fk']);
    }
    //else if (isset($_SESSION["role"]) && $_SESSION["role"] === "end") {
    //    $aFilter->AddFilterPair("iduser", $_SESSION["iduser"]);
    //}

    $crud = new Credify($dbconn, $aTable, $aFilter);
    
    if ( !empty($_POST) && $validationResult == NULL) {
        $result = $crud->InsertRow();
        if (!$result) {
         echo "<p>Error: Unable to add record. Check the format of input. Click the back button of your browser to try again.</p>";
         exit;
        }
        $aFilter = $crud->filter; // insert row changes the filter to be the row just inserted.
    }

    $context = new Context($dbconn);

    $subtitle = $context->getRowTitle($aTable, $aFilter);
    $template->Header($aTable->displayTablename() . ($subtitle ? ": " . $subtitle : ""));

    $context->show($aTable, $aFilter);

    echo("<p><a href='./read.php?t=" . $_GET["t"] . "'>View whole table</a></p>");
    
    echo "<div class='warnings'><p></p></div>";
    
    if ( !empty($_POST) ) {
     if ( $validationResult != null) {
      echo "<p class='warning'>" . $validationResult . "</p>";
     } else {
      if ($result) {
       echo "<p>Record Added</p>"; 
       
       echo '<table id="readtable" class="admin table">';
       $crud->AllColumns($aTable->tablename);
       $crud->AllRows(NULL);
       echo '</table>';
      }
     else {
      echo "<p>Error: No record inserted.</p>";
     }
    }
    }

    if (empty($_POST)) {
     $crud->CreateFormElements();
    }
    $template->Footer();
    
?>
