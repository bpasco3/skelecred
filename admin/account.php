<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    include 'Credify.php';
    include 'DBConn.php';
    include 'Template.php';
    include 'access/AccountHandler.php';
    include 'Filters.php';
    require_once 'access/Validation.php';
   
    $dbconn = new DBConn();

    $aTable = new TableDetails("user", $dbconn);

    $aFilter = new Filters();

    $template = new Template();
    $template->Header("Create Account");

    $acc = new AccountHandler($dbconn);

    // if user got past the antirobot animals, validate and show the form.
    if (isset($_POST["username"])) {
       // if (count($_POST) > 1) {

       if ((count($_POST) > 1) && !isset($_POST['animalcode'])) {
            $uvalid = new VUniqueUsername("username");
            $aFilter->AddValidator($uvalid);
            $pvalid = new VPassword("pw");
            $aFilter->AddValidator($pvalid);
            $validationResult = $aFilter->AddFormInputs($_POST);

        } else { // user did not pass robot and username check, try again

            if ($_POST['animalcode'] / 13 != $_POST['animal']) {
                echo "<p class='warnings'>Animal Vs Robot. Animal wins. If you are human please try again, an animal you know might appear.</p>";
                $template->Footer();
                exit;
            }
            if (strlen($_POST["username"]) < 6) {
                echo "<p class='warnings'>Username must be more than 6 characters.</p>";
                $acc->UserNameForm();
            } else  {
                       echo "2" ;
                $unFilter = new Filters();
                       echo "25" ;
                $unFilter->AddFilterPair("username",$_POST["username"]);
                       echo "3" ;
                if ($acc->UsernameExists($unFilter)) {
                    echo "<p class='warnings'>Username already exists, please try another.</p>";
                    "";
                                    $acc->UserNameForm();
                } else {
                    $acc->CreateAccountForm($_POST["username"]);
                }
            }
                   echo "4" ;
        }

    } else { // user passed robots show the username form

        $acc->UserNameForm();
        $template->Footer();
        exit;
    }

    $dummy = new User(0, "ContextAdmin", "admin");
    $aFilter->AddUser($dummy);
    $crud = new Credify($dbconn, $aTable, $aFilter);

    // user has submitted the full username form, double check things and insert the user.
    if ( count($_POST) > 1 && !isset($_POST['animalcode']) && $validationResult == NULL) {
        // this check of username again is to prevent a reload of the post page creating multiple accounts.

        $aFilter->AddFilterPair("username",$aFilter->modpairs["username"]);
        if ($acc->UsernameExists($aFilter)) {
                    echo "<p class='warnings'>Username already exists, please try another.</p>";
                    "";
                                    $acc->UserNameForm();
                                    $template->Footer();
                                    exit;
                }

        $result = $crud->InsertRow();

        if (!$result) {
         echo "<p>Error: Unable to add account. Check the format of input. Click the back button of your browser to try again.</p>";
         exit;
        }
        // SUCCESS FINALLY
        $aFilter = $crud->filter; // insert row changes the filter to be the row just inserted.
        $authFilter = new Filters();

        $authFilter->AddModifyPair("username", $_POST["username"]);
        $authFilter->AddModifyPair("pw", $_POST["pw"]);
        $acc->Authenticate($authFilter);

    }

echo "<div class='warnings'><p></p></div>";
    
    if (( count($_POST) > 1 ) && !isset($_POST['animalcode'])) {
        if ( $validationResult != null) {
            echo "<p class='warnings'>" . $validationResult . ". Click your browser's back button and try again.</p>";
        } else {
            if ($result) {
                echo "<p class='warnings'>Account Created</p>"; 
            }
            else {
                echo "<p class='warnings'>Error: No record inserted.</p>";
            }
        }
    }

    $template->Footer();
    
?>