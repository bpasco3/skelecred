<?php
// Skelecred (c) William Pascoe, 2016
    include 'DBConnCore.php';
    
class DBConn extends DBConnCore {

    function GetUser($filter) {
        $db = $this->GetDB()->prepare("SELECT iduser, username, role, displayname, pw FROM user WHERE username = ?" );
        $db->execute(array($filter->modpairs["username"]));
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }
    function GetUserById($filter) {
        //var_dump($filter->modpairs["username"]);
        $db = $this->GetDB()->prepare("SELECT iduser, username, role, displayname, pw FROM user WHERE iduser = ?" );
        $db->execute(array($filter->modpairs["iduser"]));
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }
    function UserExists($filter) {
        $db = $this->GetDB()->prepare("SELECT username FROM user WHERE username = ?" );
        $db->execute(array($filter->filterpairs["username"]));
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }
    function SetP($filter) {
        $db = $this->GetDB()->prepare("UPDATE user SET pw = ? where iduser = ?" );
        $db->execute(array($filter->modpairs["pw"],$filter->modpairs["iduser"]));
        return $db->fetchAll(PDO::FETCH_ASSOC);
    }

}
?>