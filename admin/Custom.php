<?php
// Skelecred (c) William Pascoe, 2016

    include 'Crudify.php';
    
class Custom extends Crudify {

  /*      public function __construct ( ) {
                $this->dbConnect = new DBConnector();
        }
     */

  // called from steps.php
    function AllStepsByTissue($tablename, $col ,$rowid) {

        if (!isset($rowid)|| !isset($col)){
            return;
        }
        
        switch ($col) {
        case 'b':
            $results = $this->dbConnect->GetRowByBarcode($rowid);
            break;
        case 'r':
            $results = $this->dbConnect->GetRow($this->table,$this->filter);
            break;
        default:
            return;
            break;
        }
        
        
//echo ("<h2>" . var_dump($rowid) . "</h2>");
// Show the tissue details
                foreach ($results as $result) {
                    echo "<div id='steptissuedetail' class='step'>
        <div id='steptissuedetailhead' class='stephead'><p>Tissue Details</p></div>
        <div id='steptissuedetaildetails' class='stepdetails'>";
        
                        foreach ($this->dbConnect->GetColumns($tablename) as $col) {
                                echo "<p>" . $col["Field"] .": " . $result[$col["Field"]] . "</p>";
                        }
                        echo $this->EditSnippet("tissue",$rowid);
                        echo $this->DeleteSnippet("tissue",$rowid);
                        
                    echo "</div></div>"; 
                        // List the sorted Steps
                        $this->GetStepsByTissueId($result["idtissue"]);
                }
    }
            
    

    function GetStepsByTissueId($tid){

        $aTable = new TableDetails("steptype", $this->dbConnect);
       // $filterPdo = $this->filter;
        
        $steptypes = $this->dbConnect->GetAllRows($aTable, NULL);
     //   echo "<div>APE0 " . var_dump($steptypes) . "</div>";

     
        $allsteps = [];
        foreach ($steptypes as $steptype) {
            $allsteps = array_merge($allsteps, $this->dbConnect->GetStep($steptype["stepname"],$tid));
        }

            usort($allsteps, array($this, "sortsteps"));
            $this->listSteps($allsteps);

    }
    
    function listSteps($steps){
        
            foreach ($steps as $step) {
                $stepTable = new TableDetails($step["stepname"],$this->dbConnect);
                echo "<div class='step' id='" . $step["stepname"] . "'><div class='stephead'><p>" . $step["stepdisplayname"] . "</p></div>
                <div class='stepdetails'>";
                //  echo "<div>PPP P " . var_dump($step) . "</div>";
                foreach ($stepTable->columnInfo as $colc) {
                    echo "<p>" . $colc["Field"] .": " . $step[$colc["Field"]] . "</p>";
                }
                echo "<p>" . $this->EditSnippet($step["stepname"],$step[$stepTable->primaryKey]) . "</p>";
                echo "<p>" . $this->DeleteSnippet($step["stepname"],$step[$stepTable->primaryKey]) . "</p>";
                echo "</div></div>";
            }
    }
    
    function GetContext() {
        
    }
    function GetTissueByStep(){
     //   $this->dbConnect->GetTissueByStep();
     //   echo "<a href='steps.php?t=tissue&r=" .  . "'>Tissue</a> ";
    }
    function GetProjectByTissue() {
    }
    function GetUserByProject(){
        
    }
    
    function AddStepsOrdered() {
        /*
         *     // Manually ordered list of options to add a step.
        echo "<div id='addstep' class='step'>
        <div id='addstephead' class='stephead'><p>Add Step</p></div>
        <div id='addstepdetails' class='stepdetails'><p>Choose step to add:</p>";
            foreach ($steptypes as $steptype) {
                echo "<p><a href='./create.php?t=" . $steptype["stepname"] . "&amp;f=idtissue&amp;fk=" . $tid . "'>" . $steptype["stepdisplayname"] . "</a></p>";
            }
            echo "</div></div>";
            */
        echo("
             <div id='addstep' class='step'>
    <div id='addstephead' class='stephead'><p>Add Step</p></div>
        <div id='addstepdetails' class='stepdetails'><p>Choose step to add. The usual order of steps is:</p>
        <ol>
        <li><a href='./create.php?t=hydrogelincubation&amp;f=idtissue&amp;fk=3'>Hydrogel Incubation</a></li>
        <li><a href='./create.php?t=polymerisation&amp;f=idtissue&amp;fk=3'>Polymerisation</a></li>
        <li><a href='./create.php?t=wash&amp;f=idtissue&amp;fk=3'>Wash</a></li>
        <li><a href='./create.php?t=clearing&amp;f=idtissue&amp;fk=3'>Clearing</a></li>
        <li><a href='./create.php?t=wash&amp;f=idtissue&amp;fk=3'>Wash</a></li>
        <li><a href='./create.php?t=molecularlabelling&amp;f=idtissue&amp;fk=3'>Molecular Labelling</a></li>
        <li><a href='./create.php?t=refractiveindexmatching&amp;f=idtissue&amp;fk=3'>Refractive Index Matching</a></li>
        <li><a href='./create.php?t=wash&amp;f=idtissue&amp;fk=3'>Wash</a></li>
        <li><a href='./create.php?t=storage&amp;f=idtissue&amp;fk=3'>Storage</a></li>
        <li><a href='./create.php?t=vacuum&amp;f=idtissue&amp;fk=3'>Vacuum</a></li>
        <li><a href='./create.php?t=dropfix&amp;f=idtissue&amp;fk=3'>Dropfix</a></li>
        </ol>
        </div>
</div>");
    }
    
         // usort sorting function for all the steps
    function sortsteps($a, $b) {
        if ($a["stepcreated"] == $b["stepcreated"]) {
            return 0;
        }
        return ($a["stepcreated"] < $b["stepcreated"]) ? -1 : 1;
    }
}
?>