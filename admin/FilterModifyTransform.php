<?php

class FilterModifyTransform {

    public $fields = '';
    public $values = [];
    public $qMarks = '';

    public function __construct ($query) {

            if ( !empty($query)) {
                $values = array_values($query);
                $this->Nullify($values);
                $afields = array_keys($query);
                $aqmarks = array_keys($query);

                foreach ($aqmarks as &$key) {
                    $key = " ? ";
                }
                $fields = implode(" , ", $afields);
                $qmarks = implode(" , ", $aqmarks);
            }
    }

        function Nullify(&$listofvalues) {
                foreach($listofvalues as &$listitem) {
                        if ($listitem == NULL) { // == regards empty string as Null. We need to set empty string to actually be null, so that integer fields get updated to null instead of an empty string which gives an error.
                                $listitem = NULL;
                        }
                }
        }
    
}
?>